#!/usr/bin/env bash

cd $LOGS_TMP_PATH
TODAY_DATE=$(date +%Y-%m-%d)

LOG_FILE=${TODAY_DATE}-access.log
aws s3 cp s3://${LOGS_S3_BUCKET}/${LOGS_S3_FOLDER}/${LOG_FILE} ./${LOG_FILE} --quiet
if [ ! -f ./${LOG_FILE} ]; then
    touch ./${LOG_FILE}
fi

cat /var/log/access.log | grep $(date +%d/%b/%Y:) > ./today.log
diff --changed-group-format='%>' --unchanged-group-format='' $LOG_FILE today.log > new.log
cat new.log >> $LOG_FILE
aws s3 cp ./${LOG_FILE} s3://${LOGS_S3_BUCKET}/${LOGS_S3_FOLDER}/${LOG_FILE} --quiet

rm today.log new.log ${LOG_FILE}